window.rblDev = {};

$().ready(function(){
	$('#err-msg').hide();
	$('#scs-msg').hide();
	clrLists();
	$('#btn-clear').on('click',function(){
		clrLists();
	});
	
	$('#btn-add').on('click',function(){
		addItems();
	});
	
	$('#btn-rm').on('click',function(){
		rmItem();
	});
	
	$('#btn-ex-xml').on('click',function(){
		exXml();
	});
	
	$('#btn-srt-name').on('click',function(){
		srtName();
	});
	
	$('#btn-srt-value').on('click',function(){
		srtValue();
	});

	dialog = $( "#export-form" ).dialog({
      autoOpen: false,
      height: 400,
      width: 500,
      modal: true,
      buttons: {
        Close: function() {
          dialog.dialog( "close" );
        }
      }
    });

});

function addItems(){
	var added = 0;
	$('input').each(function(child){
		if($(this)[0].value.match(/^[a-z|0-9]+=[a-z|0-9]+$/i)){
			console.log("Good: " + $(this)[0].value);
			rblDev.kvl.push($(this)[0].value);
			$(this)[0].value = '';
			added++;
		}else if($(this)[0].value.length > 0){
			invalidate($(this),'Entries must match format key=value');
		}
	});
	popList();
	showSuccess("Added " + added + " items");
}

function rmItem(){
	var html = $('#item-lst').find(".active").html();
	if(html.match(/^[a-z|0-9]+=[a-z|0-9]+$/i)){
		rblDev.kvl.splice(rblDev.kvl.indexOf(html),1);
		popList();
	}
	showSuccess(html + " removed");

}



function exXml(){
	var xml = "<?xml version='1.0'?>\n<pairs>\n";
	rblDev.kvl.forEach(function(item){
		xml += "  <pair>\n";
		xml += "    <key>" + item.split("=")[0] + "</key>\n";
		xml += "    <value>" + item.split("=")[1] + "</value>\n";
		xml += "  </pair>\n";
	});
	xml += "</pairs>"
	$("#ex-output").html(xml);
	dialog.dialog("open");
}

function srtName(){
	rblDev.kvl.sort();
	popList();
	showSuccess("Sorted by name");
}

function srtValue(){
	rblDev.kvl.sort(function(a,b){
		if(a.split("=")[1] < b.split("=")[1]){
			return -1;
		}else if(b.split("=")[1] < a.split("=")[1]){
			return 1;
		}
		return 0;
	});
	popList();
	showSuccess("Sorted by value");
}

function clrLists(){
	rblDev.kvl = [];
	popList();
	clrToAddList();
	showSuccess("List cleared");
}

function popList(){
	$('#item-lst').empty();
	rblDev.kvl.forEach(function(item){
		$('#item-lst').append('<li class="list-group-item list-group-item-action p-0">' + item + '</li>');
	});
	for(var i=0;i < 19 - rblDev.kvl.length ;i++){
		$('#item-lst').append('<li class="list-group-item list-group-item-action"></li>');
	}
	$('#item-lst').children().on('click',function(){
		$('#item-lst').children().removeClass('active');
		$(this).addClass('active');
	});
}

function clrToAddList(){
	$('#toadd-lst').empty();
	for(var i=0;i<18;i++){
		$('#toadd-lst').append('<div><input type="text" class="form-control p-0" placeholder="key=value"><div class="invalid-feedback"></div></div>');
	}
	$('#toadd-lst').children().children().on('keydown',function(event){
		$(this).removeClass('is-invalid')
		if(event.key == '=' && $(this)[0].value.match(/=/)){
			event.preventDefault();
			showWarn("Only one = sign please");
		}else if(!event.key.match(/[0-9|a-z|=]/i)){
			event.preventDefault();
			showWarn("Alphanumeric Characters Only Please");
		}
	});
}

function invalidate(item,msg){
	item.addClass('is-invalid');
	item.parent().find(".invalid-feedback").html(msg);
}

function showWarn(msg){
	$('#err-msg').html(msg);
	$('#err-msg').show();
	$('#err-msg').fadeOut(1500);
}

function showSuccess(msg){
	$('#scs-msg').html(msg);
	$('#scs-msg').show();
	$('#scs-msg').fadeOut(2000);
}